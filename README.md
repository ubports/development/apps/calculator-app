# Lomiri Calculator Application

The Lomiri Calculator Application is a powerful and easy to use calculator for
[Ubuntu Touch](https://ubuntu-touch.io/), with calculations history and formula
validation.

[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/calculator.ubports)

<img src="screenshot.png" alt="screenshot" width="200"/>

## Ubuntu Touch

This app is a core app for the [Ubuntu Touch](https://ubuntu-touch.io/) mobile
OS developed by the [UBports](https://ubports.com/).

## Reporting Bugs and Requesting Features

Bugs and feature requests can be reported via our
[bug tracker](https://gitlab.com/ubports/development/apps/lomiri-calculator-app/issues).

## Translating

You can easily contribute to the localization of this project (i.e. the
translation into your language) by visiting (and signing up with) the
[Hosted Weblate service](https://hosted.weblate.org/projects/lomiri/lomiri-calculator-app)

The localization platform of this project is sponsored by Hosted Weblate
via their free hosting plan for Libre and Open Source Projects.

## Developing

To get started developing Calculator install
[Clickable](https://clickable-ut.dev/en/latest/).
Then you can run `clickable` from the root of this repository to build and
deploy Calculator on your attached Ubuntu Touch device.  Run `clickable
desktop` to test Calculator on your desktop.

### Contributing

When contributing to this repository, please file a merge request on the
project where your changes can be discussed with the maintainers.
Please note we have a [code of conduct](https://ubports.com/code-of-conduct),
please follow it in all your interactions with the project.

### Maintainer

This Ubuntu Touch core app is being maintained by the UBports core developers.

See also the list of
[contributors](https://gitlab.com/ubports/development/apps/lomiri-calculator-app/-/graphs/main)
who participated in this project.

### Releasing

New releases are automatically pushed to the OpenStore via the GitLab CI. To
make a new release a maintainer must:

- Increment the version in the manifest.json
- Create a new git commit, the commit message will become part of the automatic
  changelog in the OpenStore.
- Tag the commit with a tag starting with `v` (for example: `git tag v1.2.3`)
- Push the new commit and tag to GitLab: `git push && git push --tags`
- The GitLab CI will build and publish the new release to the OpenStore.

### Useful Links

- [UBports App Dev Docs](http://docs.ubports.com/en/latest/appdev/index.html)
- [UBports SDK Docs](https://api-docs.ubports.com/)
- [UBports](https://ubports.com/)
- [Ubuntu Touch](https://ubuntu-touch.io/)
- [Math.js Library](https://mathjs.org/)

## Donating

If you love UBports and it's apps please consider dontating to the [UBports
Foundation](https://ubports.com/donate). Thanks in advance for your generous
donations.

## License

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License version 3, as published by the Free
Software Foundation.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY
QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
License for more details.
You should have received a copy of the GNU General Public License along with
this program.  If not, see http://www.gnu.org/licenses/.
