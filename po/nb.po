# Norwegian Bokmal translation for ubuntu-calculator-app
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the ubuntu-calculator-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-calculator-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-09-28 13:08+0000\n"
"PO-Revision-Date: 2023-01-07 02:50+0000\n"
"Last-Translator: Allan Nordhøy <epost@anotheragency.no>\n"
"Language-Team: Norwegian Bokmål <https://hosted.weblate.org/projects/lomiri/"
"lomiri-calculator-app/nb_NO/>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.15.1-dev\n"
"X-Launchpad-Export-Date: 2017-04-05 07:42+0000\n"

#: lomiri-calculator-app.desktop.in:3 app/lomiri-calculator-app.qml:285
msgid "Calculator"
msgstr "Kalkulator"

#: lomiri-calculator-app.desktop.in:4
msgid "A calculator for Lomiri."
msgstr "Kalkulator for Lomiri."

#: lomiri-calculator-app.desktop.in:5
msgid "math;addition;subtraction;multiplication;division;"
msgstr "matte;addisjon;subtraksjon;multiplikasjon;divisjon;"

#: lomiri-calculator-app.desktop.in:7
#, fuzzy
msgid "/usr/share/lomiri-calculator-app/lomiri-calculator-app.svg"
msgstr "/usr/share/lomiri-calculator-app/lomiri-calculator-app.svg"

#: app/engine/formula.js:179
msgid "NaN"
msgstr "I/NUM"

#: app/lomiri-calculator-app.qml:294
msgid "Cancel"
msgstr "Avbryt"

#: app/lomiri-calculator-app.qml:306
msgid "Select All"
msgstr "Velg alt"

#: app/lomiri-calculator-app.qml:306
msgid "Select None"
msgstr "Velg ingen"

#: app/lomiri-calculator-app.qml:313 app/lomiri-calculator-app.qml:429
msgid "Copy"
msgstr "Kopier"

#: app/lomiri-calculator-app.qml:321 app/lomiri-calculator-app.qml:416
msgid "Delete"
msgstr "Slett"

#: app/lomiri-calculator-app.qml:439
msgid "Edit"
msgstr "Rediger"

#: app/lomiri-calculator-app.qml:452
msgid "Edit Result"
msgstr "Rediger resultat"

#: app/lomiri-calculator-app.qml:823
msgid "Functions"
msgstr "Funksjoner"

#. TRANSLATORS Natural logarithm symbol (logarithm to the base e)
#: app/ui/BottomEdgePage.qml:55 app/ui/LandscapeKeyboard.qml:39
msgid "log"
msgstr "logg"

#. TRANSLATORS Modulo operation: Finds the remainder after division of one number by another
#: app/ui/BottomEdgePage.qml:59 app/ui/LandscapeKeyboard.qml:46
msgid "mod"
msgstr "mod"

#~ msgid "Favorite"
#~ msgstr "Favoritt"

#~ msgid "Add to favorites"
#~ msgstr "Legg til som favoritt"

#~ msgid "dd MMM yyyy"
#~ msgstr "d. MMM yyyy"

#~ msgid "No favorites"
#~ msgstr "Ingen favoritter"

#~ msgid ""
#~ "Swipe calculations to the left\n"
#~ "to mark as favorites"
#~ msgstr ""
#~ "Dra utregninger mot venstre\n"
#~ "for å markere dem som favoritter"

#~ msgid "Just now"
#~ msgstr "Nå nettopp"

#~ msgid "Today "
#~ msgstr "I dag "

#~ msgid "hh:mm"
#~ msgstr "HH:mm"

#~ msgid "Yesterday"
#~ msgstr "I går"

#~ msgid "Skip"
#~ msgstr "Hopp over"

#~ msgid "Welcome to Calculator"
#~ msgstr "Velkommen til Kalkulator"

#~ msgid "Enjoy the power of math by using Calculator"
#~ msgstr "Bruk Kalkulator og dra nytte av matematikkens kraft"

#~ msgid "Copy formula"
#~ msgstr "Kopier formler"

#~ msgid "Long press to copy part or all of a formula to the clipboard"
#~ msgstr ""
#~ "Trykk lenge for å kopiere hele eller deler av formler til utklippstavla"

#~ msgid "Enjoy"
#~ msgstr "Kos deg"

#~ msgid "We hope you enjoy using Calculator!"
#~ msgstr "Vi håper du får nytte av kalkulatoren!"

#~ msgid "Finish"
#~ msgstr "Ferdig"

#~ msgid "Scientific keyboard"
#~ msgstr "Vitenskapelig tastatur"

#~ msgid "Access scientific functions with a left swipe on the numeric keypad"
#~ msgstr ""
#~ "Dra til venstre på nummertastaturet for å bruke vitenskapelige funksjoner"

#~ msgid "Scientific View"
#~ msgstr "Vitenskapelig visning"

#~ msgid "Rotate device to show numeric and scientific functions together"
#~ msgstr ""
#~ "Roter enheten for å vise numeriske og vitenskapelige funksjoner samtidig"

#~ msgid "Delete item from calculation history"
#~ msgstr "Slett element fra utregningshistorikk"

#~ msgid "Swipe right to delete items from calculator history"
#~ msgstr "Dra til høyre for å slette elementer fra kalkulator-historikken"

#~ msgid "Delete several items from calculation history"
#~ msgstr "Slett flere elementer fra utregningshistorikk"

#~ msgid "Long press to select multiple calculations for deletion"
#~ msgstr "Trykk lenge for å merke flere utregninger for sletting"

#~ msgid "Delete whole formula at once"
#~ msgstr "Slett hel formel på én gang"

#~ msgid "Long press '←' button to clear all formulas from input bar"
#~ msgstr "Trykk lenge på «←» for å tømme alle formler fra inndata-linja"

#~ msgid "Edit item from calculation history"
#~ msgstr "Rediger element i utregningshistorikk"

#~ msgid "Swipe left and press pencil to edit calculation"
#~ msgstr "Dra til venstre og trykk på blyant for å redigere utregning"

#~ msgid "Add new favourite"
#~ msgstr "Legg til ny favoritt"

#~ msgid "Swipe left and press star to add calculations to favourites view"
#~ msgstr ""
#~ "Dra til venstre og trykk på stjerne for å legge til utregninger fra "
#~ "favoritter"

#~ msgid "Click in the middle of a formula to edit in place"
#~ msgstr "Trykk i midten av en formel for å redigere den"
