# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Canonical Ltd.
# This file is distributed under the same license as the lomiri-calculator package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-calculator\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-09-28 13:08+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: wo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: lomiri-calculator-app.desktop.in:3 app/lomiri-calculator-app.qml:285
msgid "Calculator"
msgstr ""

#: lomiri-calculator-app.desktop.in:4
msgid "A calculator for Lomiri."
msgstr ""

#: lomiri-calculator-app.desktop.in:5
msgid "math;addition;subtraction;multiplication;division;"
msgstr ""

#: lomiri-calculator-app.desktop.in:7
msgid "/usr/share/lomiri-calculator-app/lomiri-calculator-app.svg"
msgstr ""

#: app/engine/formula.js:179
msgid "NaN"
msgstr ""

#: app/lomiri-calculator-app.qml:294
msgid "Cancel"
msgstr ""

#: app/lomiri-calculator-app.qml:306
msgid "Select All"
msgstr ""

#: app/lomiri-calculator-app.qml:306
msgid "Select None"
msgstr ""

#: app/lomiri-calculator-app.qml:313 app/lomiri-calculator-app.qml:429
msgid "Copy"
msgstr ""

#: app/lomiri-calculator-app.qml:321 app/lomiri-calculator-app.qml:416
msgid "Delete"
msgstr ""

#: app/lomiri-calculator-app.qml:439
msgid "Edit"
msgstr ""

#: app/lomiri-calculator-app.qml:452
msgid "Edit Result"
msgstr ""

#: app/lomiri-calculator-app.qml:823
msgid "Functions"
msgstr ""

#. TRANSLATORS Natural logarithm symbol (logarithm to the base e)
#: app/ui/BottomEdgePage.qml:55 app/ui/LandscapeKeyboard.qml:39
msgid "log"
msgstr ""

#. TRANSLATORS Modulo operation: Finds the remainder after division of one number by another
#: app/ui/BottomEdgePage.qml:59 app/ui/LandscapeKeyboard.qml:46
msgid "mod"
msgstr ""
